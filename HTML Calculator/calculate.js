function addInput(input)
{
	document.getElementById("answer").value += input;
}

function backspace()
{
	document.getElementById("answer").value = 
		document.getElementById("answer").value.slice(0, -1);
}

function clearDisplay()
{
	document.getElementById("answer").value = '';
}

function solve()
{
	var input = document.getElementById("answer").value;
	// Convert trig functions to proper code
	input = input.replace("sin", "Math.sin");
	input = input.replace("cos", "Math.cos");
	input = input.replace("tan", "Math.tan");

	// Convert exp to proper code
	input = input.replace("pow", "Math.pow");

	document.getElementById("answer").value = 
		eval(input);
}