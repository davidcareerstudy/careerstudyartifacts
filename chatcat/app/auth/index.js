'use strict';

const passport = require('passport');
const config = require('../config');
const h = require('../helpers');
const logger = require('../logger');
const FacebookStrategy = require('passport-facebook').Strategy;
const TwitterStrategy = require('passport-twitter').Strategy;

module.exports = () => {
	// Runs after authProcessor when done() is called
	// Create session, storing only user id
	// Note: not Facebook's 'profileId', but MongoDB's '_id'
	passport.serializeUser((user, done) => {
		done(null, user.id);
	});

	// Runs when there's a request for user data
	passport.deserializeUser((id, done) => {
		// Find the user using the _id
		h.findById(id)
			// Makes user avaliable via req.user
			.then(user => done(null, user))
			.catch(error => {
				logger.log('error', 'Error when deserializing the user: ' + error);
			});
	});

	// Facebook & Twitter gives accessToken, refreshToken, and user's profile (using Passport schema)
	// Each service has own way of returning info, so 'profile' standarizes how it's presented
	// 'profile' includes properties like 'id', 'displayName', and 'photos'
	// 'done' returns the authenticated user
	let authProcessor = (accessToken, refreshToken, profile, done) => {
		// Find a user in the local db using profile.id
		// If the user is found, return the user data using done()
		// If the user is not found, create on in the local db and return
		h.findOne(profile.id)
			.then(result => {
				if(result) {
					// User found in database
					done(null, result);
				}
				else {
					// Create a new user and return
					h.createNewUser(profile)
						.then(newChatUser => done(null, newChatUser))
						.catch(error => {
							logger.log('error', 'Error when creating new user : ' + error);
						});
				}
			});
	}

	// Accept App ID, Secret, and callback, as well as specific user info in 1st argument
	// Facebook returns info given in authProcessor
	passport.use(new FacebookStrategy(config.fb, authProcessor));
	passport.use(new TwitterStrategy(config.twitter, authProcessor));
}